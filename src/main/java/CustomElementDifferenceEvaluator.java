import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xmlunit.diff.Comparison;
import org.xmlunit.diff.ComparisonResult;
import org.xmlunit.diff.ComparisonType;
import org.xmlunit.diff.DifferenceEvaluator;

import java.math.BigDecimal;

class CustomElementDifferenceEvaluator implements DifferenceEvaluator {

    @Override
    public ComparisonResult evaluate(Comparison comparison, ComparisonResult outcome) {
        Comparison bb = comparison;
        if (outcome == ComparisonResult.EQUAL) return outcome; // only evaluate differences.
        final Node controlNode = comparison.getControlDetails().getTarget();
        final Node testNode = comparison.getTestDetails().getTarget();
        try {
            /*if (controlNode == null || testNode == null) {

                if (controlNode == null) {
                    if(testNode.getNodeName().equalsIgnoreCase("ext:data")) {
                        String value = testNode.getFirstChild().getTextContent();

                        Comparison.Detail control = comparison.getControlDetails();
                        Comparison.Detail test = comparison.getTestDetails();

                        Comparison finalComparision = new Comparison(comparison.getType(),
                                control.getTarget(), control.getXPath(), control.getValue(),control.getParentXPath(),
                                test.getTarget(), test.getXPath(), value, test.getParentXPath());

                    }
                }

            } else */if (controlNode.getParentNode() instanceof Element && testNode.getParentNode() instanceof Element) {
                Element controlElement = (Element) controlNode.getParentNode();
                Element testElement = (Element) testNode.getParentNode();
                final String controlValue = controlElement.getTextContent();
                final String testValue = testElement.getTextContent();

                BigDecimal source = new BigDecimal(controlValue);
                BigDecimal destination = new BigDecimal(testValue);
                if (source.compareTo(destination) == 0) {
                    return ComparisonResult.SIMILAR;
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }


        if (comparison.getType().equals(ComparisonType.CHILD_NODELIST_SEQUENCE) ||
        comparison.getType().equals(ComparisonType.CHILD_NODELIST_LENGTH)) {
            return ComparisonResult.SIMILAR;
        }

        return outcome;
    }
}
