import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public class UI extends Application {

    private Stage primaryStage;
    private final static String FOLDER_SELECT_LABEL= "...";
    Alert a;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
       primaryStage.setTitle("Registration Form JavaFX Application");


        // Create the registration form grid pane
        GridPane gridPane = createRegistrationFormPane();
        // Add UI controls to the registration form grid pane
        addUIControls(gridPane);
        // Create a scene with registration form grid pane as the root node
        Scene scene = new Scene(gridPane, 800, 500);
        // Set the scene in primary stage
        primaryStage.setScene(scene);

        primaryStage.show();

    }


    private GridPane createRegistrationFormPane() {
        // Instantiate a new Grid Pane
        GridPane gridPane = new GridPane();

        // Position the pane at the center of the screen, both vertically and horizontally
        gridPane.setAlignment(Pos.CENTER);

        // Set a padding of 20px on each side
        gridPane.setPadding(new Insets(40, 40, 40, 40));

        // Set the horizontal gap between columns
        gridPane.setHgap(10);

        // Set the vertical gap between rows
        gridPane.setVgap(10);

        // Add Column Constraints

        // columnOneConstraints will be applied to all the nodes placed in column one.
        ColumnConstraints columnOneConstraints = new ColumnConstraints(100, 100, Double.MAX_VALUE);
        columnOneConstraints.setHalignment(HPos.RIGHT);

        // columnTwoConstraints will be applied to all the nodes placed in column two.
        ColumnConstraints columnTwoConstrains = new ColumnConstraints(200,200, Double.MAX_VALUE);
        columnTwoConstrains.setHgrow(Priority.ALWAYS);

        gridPane.getColumnConstraints().addAll(columnOneConstraints, columnTwoConstrains);

        return gridPane;
    }

    private void addUIControls(GridPane gridPane) {

        DirectoryChooser directoryChooser = new DirectoryChooser();
        //directoryChooser.setInitialDirectory(new File("src"));

        // Add Header
        Label headerLabel = new Label("Folder Compare");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0,0,2,1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(20, 0,20,0));

        // Add source row
        gridPane.add(new Label("Source Dir : "), 0,1);

        TextField sourceField = new TextField();
        sourceField.setPrefHeight(40);
        gridPane.add(sourceField, 1,1);

        Button sourceButton = new Button(FOLDER_SELECT_LABEL);
        sourceButton.setOnAction(e -> {
            File sourceDirectory = directoryChooser.showDialog(primaryStage);
            sourceField.setText(sourceDirectory.getAbsolutePath());
            //System.out.println(sourceDirectory.getAbsolutePath());
        });
        gridPane.add(sourceButton, 2, 1);


        // Add destination row
        gridPane.add(new Label("Destination Dir : "), 0, 2);

        TextField destinationField = new TextField();
        destinationField.setPrefHeight(40);
        gridPane.add(destinationField, 1, 2);

        Button destinationButton = new Button(FOLDER_SELECT_LABEL);
        destinationButton.setOnAction(e -> {
            File destinationDirectory = directoryChooser.showDialog(primaryStage);
            destinationField.setText(destinationDirectory.getAbsolutePath());
        });
        gridPane.add(destinationButton, 2, 2);

        // Add result row
        gridPane.add(new Label("Result Dir : "), 0, 3);

        TextField resultField = new TextField();
        resultField.setPrefHeight(40);
        gridPane.add(resultField, 1, 3);


        Button resultButton = new Button(FOLDER_SELECT_LABEL);
        resultButton.setOnAction(e -> {
            File resultDirectory = directoryChooser.showDialog(primaryStage);
            resultField.setText(resultDirectory.getAbsolutePath());
            //System.out.println(destinationDirectory.getAbsolutePath());
        });
        gridPane.add(resultButton, 2, 3);



        try {
            String def = System.getProperty("user.home") + "/Desktop/work";
            FileReader reader = new FileReader(def + "/xmlcompare/default.properties");

            Properties p = new Properties();
            p.load(reader);
            String a = p.getProperty("source");
            String b = p.getProperty("destination");
            String c = p.getProperty("result");
        } catch (Exception e) {
            e.printStackTrace();
        }



        // Add Submit Button
        Button submitButton = new Button("Submit");
        submitButton.setPrefHeight(40);
        submitButton.setDefaultButton(true);
        submitButton.setPrefWidth(100);

        submitButton.setOnAction(event -> {
            MyTest t = new MyTest();
            boolean result = t.bb();
            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", ""+result);


            if(sourceField.getText().isEmpty()) {
                showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please provide Source Dir");
                return;
            }
            if(destinationField.getText().isEmpty()) {
                showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please provide Destination Dir");
                return;
            }
            if(resultField.getText().isEmpty()) {
                showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please provide Result Dir");
                return;
            }
            showAlert(Alert.AlertType.CONFIRMATION, gridPane.getScene().getWindow(), "Registration Successful!", "Welcome " + sourceField.getText());
        });


        Button resetButton = new Button("RESET");
        resetButton.setPrefHeight(40);
        resetButton.setDefaultButton(true);
        resetButton.setPrefWidth(100);

        GridPane buttonGridPane = new GridPane();
        buttonGridPane.add(submitButton,0,0);
        buttonGridPane.add(resetButton,1,0);

        GridPane.setHalignment(submitButton, HPos.CENTER);
        GridPane.setValignment(buttonGridPane, VPos.CENTER); // To align vertically in the cell
        GridPane.setMargin(buttonGridPane, new Insets(20, 0,20,0));
        buttonGridPane.setStyle("-fx-background-color: #C0C0C0;");
        gridPane.add(buttonGridPane, 1, 4);



    }

    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}