import org.apache.commons.io.FileUtils;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.*;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Logger;

public class MyTest {
    private static final String UNDER_SCORE = "_";
    private static final String SOURCE_BASE_PATH = "/Users/siddharth/Desktop/work/source/";
    private static final String DES_BASE_PATH = "/Users/siddharth/Desktop/work/destination/";
    private static final String REPORT_BASE_PATH = "/Users/siddharth/Desktop/work/result/";
    private static final String REMOVE_EXTRA_TAGS = "org.custommonkey.xmlunit.DetailedDiff";
    private static Logger logger = Logger.getLogger(MyTest.class.getName());

    private static final String SOURCE_TAG = "_stage2.xml";
    private static final String DES_TAG = "_mrr.fpml";
    private static Exception myException;
    private static int compareCount;

    public static void main(String[] args) throws Exception {
        folderSet();
    }

    public boolean bb() {
        return true;
    }

    private static void folderSet() throws Exception {

        File directoryPath = new File(SOURCE_BASE_PATH);
        String contents[] = directoryPath.list();
        List<CustomFile> sourceList = new ArrayList<>();
        List<String> sFileList = Arrays.asList(contents);
        ListIterator<String> iterator = sFileList.listIterator();
        while (iterator.hasNext()) {
            final String fullName = iterator.next();
            String lowerFileName = fullName.toLowerCase();
            if (lowerFileName.contains(SOURCE_TAG)) {
                lowerFileName = lowerFileName.replace(SOURCE_TAG, "")
                        .replace("-", "")
                        .replace("_", "");
                sourceList.add(new CustomFile(fullName, lowerFileName));
            }
        }

        File destinationPath = new File(DES_BASE_PATH);
        String dContents[] = destinationPath.list();
        List<CustomFile> destinationList = new ArrayList<>();
        List<String> dFileList = Arrays.asList(dContents);
        iterator = dFileList.listIterator();
        while (iterator.hasNext()) {
            final String fullName = iterator.next();
            String lowerFileName = fullName.toLowerCase();
            if (lowerFileName.contains(DES_TAG) && !lowerFileName.contains("ArML")) {
                lowerFileName = lowerFileName.replace(DES_TAG, "")
                        .replace("-", "")
                        .replace("_", "");
                destinationList.add(new CustomFile(fullName, lowerFileName));
            }
        }

        String time = getTime();
        String resultFinalFolder = REPORT_BASE_PATH + time;
        Files.createDirectories(Paths.get(resultFinalFolder));

        compareCount = 0;
        sourceList.forEach(file -> {

            destinationList.forEach(desFile -> {
                if (file.getSortName().equals(desFile.getSortName())) {
                    compareCount++;
                    String outputFile = file.getFullName().substring(0, file.getFullName().lastIndexOf('.'));
                    try {
                        assertXMLEquals(SOURCE_BASE_PATH + file.getFullName(), DES_BASE_PATH + desFile.getFullName(), outputFile, resultFinalFolder);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        });

        int a = compareCount;

    }

    public static final Map<String, String> NAMESPACE = new HashMap<>();
    private static int ii = 0;

    private static void assertXMLEquals(String sourceFile, String destinationFile, String outFile, String resultFinalFolder) throws Exception {
        NAMESPACE.put("p", "http://dummy.com");

        File sFile = new File(sourceFile);
        File dFile = new File(destinationFile);

        String expectedXML = FileUtils.readFileToString(sFile);
        String actualXML = FileUtils.readFileToString(dFile);

        System.out.println(expectedXML);
        System.out.println(actualXML);
        //Document doc = XMLUnit.buildTestDocument(actualXML);
        //DetailedDiff diff = new DetailedDiff(XMLUnit.compareXML(expectedXML, actualXML));

        List<String> excludeElements = Arrays.asList("foo", "baba");


        Diff diff = null;
        try {
            diff = DiffBuilder.compare(expectedXML).withTest(actualXML)
                    .withNamespaceContext(NAMESPACE)
                    .ignoreWhitespace()
                    .ignoreComments()
                    .checkForSimilar()
                    .withNodeFilter(node -> !excludeElements.contains(node.getNodeName()))
                    .withDifferenceEvaluator(
                            DifferenceEvaluators.chain(DifferenceEvaluators.Default,
                                    new CustomElementDifferenceEvaluator()))
                    .withNodeMatcher(new DefaultNodeMatcher(ElementSelectors.conditionalBuilder()
                            .whenElementIsNamed("data").thenUse(ElementSelectors.byNameAndAttributes("mode"))
                            .elseUse(ElementSelectors.byName).build()))
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
        }


        ii = 0;
        Iterable<Difference> allDifferences = diff.getDifferences();
        StringBuilder finalMsg = new StringBuilder();
        allDifferences.forEach(difference -> {
            ii = ++ii;
            String customMsg = composeCustomMsg(difference);
            finalMsg.append(ii).append(". ").append(customMsg).append("\n\n");
        });
        int allDiffSize = ii;
        //System.out.println(diff.toString());

        String reportFile = resultFinalFolder + "/" + outFile + "_" + allDiffSize + "_" + "Differences";
        File myFoo = new File(reportFile);
        FileWriter fooWriter = new FileWriter(myFoo, false); // true to append
        // false to overwrite.
        fooWriter.write(finalMsg.toString());
        fooWriter.close();
        int a = 10;
        //Assert.assertEquals("Differences found: "+ diff.toString(), 0, allDifferences.size());
    }

    private static String composeCustomMsg(Difference difference) {
        String finalMsg = difference.toString();
        Comparison comparision = difference.getComparison();
        if ("DIFFERENT".equalsIgnoreCase(difference.getResult().toString()) &&
                (ComparisonType.CHILD_LOOKUP.equals(difference.getComparison().getType()) || ComparisonType.TEXT_VALUE.equals(difference.getComparison().getType()))) {

            final Node controlNode = comparision.getControlDetails().getTarget();
            final Node testNode = comparision.getTestDetails().getTarget();
            Node finalNode;

            ComparisonType type = difference.getComparison().getType();
            String description = type.getDescription();
            String controlXPath = comparision.getControlDetails().getXPath();
            String testXPath = comparision.getTestDetails().getXPath();
            String controlValue = null;
            String testValue = null;

            if (controlNode == null || testNode == null) {

                if (controlNode != null) {
                    finalNode = controlNode;
                    controlValue = controlNode.getFirstChild().getTextContent();
                    controlXPath = getTagName(finalNode) + controlXPath;
                    testXPath = "<NULL>";
                } else {
                    finalNode = testNode;
                    testValue = testNode.getFirstChild().getTextContent();
                    controlXPath = "<NULL>";
                    testXPath = getTagName(finalNode) + testXPath;
                }
                finalMsg = String.format("Expected %s '%s' but was '%s' - comparing %s to %s", description, controlValue, testValue, controlXPath, testXPath);
            }





            else {
                controlValue = controlNode.getNodeValue();
                controlXPath = getTagName(controlNode) + controlXPath;
                testValue = testNode.getNodeValue();
                testXPath = getTagName(testNode) + testXPath;
                finalMsg = String.format("Expected %s '%s' but was '%s' - comparing %s to %s", description, controlValue, testValue, controlXPath, testXPath);

            }
        }
        return finalMsg;
    }

    private static String getTagName(Node finalNode) {
        String msg = "";
        NamedNodeMap attributes = finalNode.getAttributes();

        if (attributes != null && attributes.getLength() > 0) {
            int length = attributes.getLength();
            for (int i=0; i< length;i++){
                msg = msg + attributes.item(i) + " ";
            }
            if (msg.endsWith(" ")) {
                msg = " " + msg;
                msg = msg.substring(0, msg.length() - 1);
            }
        }
        msg = "<" + finalNode.getNodeName() + msg + "> at";
        return msg;
    }


    private static String intDateToString(int val) {
        if (val < 10)
            return "0" + val;
        else
            return "" + val;
    }

    private static String getTime() {
        LocalDateTime dateTime = LocalDateTime.now();
        return intDateToString(dateTime.getDayOfMonth()) + UNDER_SCORE +
                (dateTime.getMonth().toString()).substring(0, 3) + UNDER_SCORE +
                dateTime.getYear() + UNDER_SCORE +
                intDateToString(dateTime.getHour()) +
                intDateToString(dateTime.getMinute()) +
                intDateToString(dateTime.getSecond());
    }

}
