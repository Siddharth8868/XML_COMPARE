public class CustomFile {

    private String fullName;

    private String sortName;

    public CustomFile(String fullName, String sortName) {
        this.fullName = fullName;
        this.sortName = sortName;
    }

    public String getFullName() {
        return fullName;
    }

    public String getSortName() {
        return sortName;
    }
}
