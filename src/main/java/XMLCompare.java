import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileReader;
import java.util.*;

public class XMLCompare {

    private static final String SOURCE_BASE_PATH = "/Users/siddharth/Desktop/work/source/";

    public static void main(String[] args) {

        try {
            String def = System.getProperty("user.home") + "/Desktop";
            FileReader reader = new FileReader(def + "/xmlcompare/default.properties");
            Properties p = new Properties();
            p.load(reader);
            String namesVal = p.getProperty("names");
            List<String> fileTypes = Arrays.asList(namesVal.split(","));

            File directoryPath = new File(SOURCE_BASE_PATH);
            String[] contents = directoryPath.list();
            List<String> sFileList = Arrays.asList(contents);
            ListIterator<String> iterator = sFileList.listIterator();
            while (iterator.hasNext()) {
                final String fileName = iterator.next();
                if (!fileName.toLowerCase().contains(".xml")) continue;
                int i = 0;
                for (String fileType : fileTypes) {
                    if (fileName.toLowerCase().contains(fileType.toLowerCase())) {
                        i++;
                        String tagSetVal = p.getProperty(fileType);
                        List<String> tagList = Arrays.asList(tagSetVal.split(","));

                        File sFile = new File(SOURCE_BASE_PATH + fileName);
                        String xmlString = FileUtils.readFileToString(sFile).toLowerCase();

                        List<String> missingTags = new ArrayList<>();
                        for (String tag : tagList) {
                            if (!xmlString.toLowerCase().contains(tag.toLowerCase())) {
                                missingTags.add(tag);
                            }
                        }
                        if (missingTags.size() == 0) {
                            System.out.println("SUCCESS - " + fileName + " contains all tags");
                        } else {
                            System.out.println("FAILED - " + fileName + " missing following tags : " + missingTags);
                        }

                    }
                } // for loop ending

                if (i == 0) {
                    System.out.println(fileName + " doesn't belong to any type : " + fileTypes);
                }
                i = 0;

            } // while loop ending

        } catch (Exception e) {
            e.printStackTrace();
        }

    } //main method ending

}
